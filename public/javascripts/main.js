var app=angular.module('2doApp', []);

app.controller('tasksOverviewCtrl', function($scope, $filter){
    $scope.tasks = [
      { id: 1, name: "Einkaufen", type: "check", priority: 8 },
      { id: 2, name: "Putzen", type: "check", priority: 2 },
      { id: 3, name: "Rechnungen bezahlen", type: "check", priority: 10 },
      { id: 4, name: "Drainage legen", type: "text", priority: 10 }
    ];

    $scope.finished=function(id) {


    	$filter('filter')($scope.tasks, {id: id })[0].finished = true;
    };
  });